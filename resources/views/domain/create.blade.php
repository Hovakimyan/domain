@extends('layout')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Create New Domain
                </div>
                <form action="{{route('store')}}" method="post">
                @csrf
                <div class="form-group col-lg-4 col-lg-offset-4">
                    <label>Enter Your Domain</label>
                    <input type="hidden" name="domain" value="0">
                    <input type="text" name="name" data-action="create" class="form-control" autofocus>
                    <p class="help-block domain-info "></p>
                </div>
                <div class="col-lg-4 col-lg-offset-4" id="buy_div">
                    <div class="form-group text-center">
                        <button class="btn btn-success buy-domain" disabled type="submit">BUY</button>
                    </div>
                </div>
                </form>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <style>
        .error {
            color: red;
        }

        .success {
            color: green;
        }
    </style>
@endsection
