@extends('layout')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    My Domains
                </div>
                @if (Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{Session::get('success')}}
                </div>
                @endif
                <a class="btn btn-success" style="float: right; margin-right: 15px;margin-bottom: 10px;margin-top: 5px" href="{{route('create')}}">Add New Domain</a>
                <div class="panel-body">

                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Domain</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Deleted At</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($domains as $domain)
                        <tr class="odd gradeX">
                            <td>{{$domain->id}}</td>
                            <td>{{$domain->name}}</td>
                            <td>{{$domain->created_at}}</td>
                            <td>{{$domain->updated_at}}</td>
                            <td>{{$domain->deleted_at}}</td>
                            <td class="center">
                                <div style="margin: 0 auto; width: 100%;text-align: center;">
                                    @if (!$domain->deleted_at)
                                    <a href="{{route('edit', ['id' => $domain->id])}}" class="btn btn-info btn-xs" data-tooltip="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
                                    @endif
                                    <button type="button" class="btn btn-{{$domain->deleted_at ? 'success' : 'danger'}} btn-xs delete-domain" data-action="{{ route($domain->deleted_at ? 'restore' : 'destroy', ['id'=> $domain->id]) }}" data-method="{{$domain->deleted_at ? 'PATCH' : 'DELETE'}}" data-toggle="modal" data-target="#deleteModal" data-tooltip="tooltip" title="Delete"><i class="fa fa-trash"></i></button>

                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>



    <div class="modal" tabindex="-1" role="dialog" id="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" id="deleteDomain" action="">
                    @method('DELETE')
                    @csrf
                    <div class="modal-header text-default">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Delete Domain</h4>
                    </div>
                    <div class="modal-body">
                        <h3>Are you sure you want to <span class="type"></span> the Domain?</h3>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-outline btn-success confirm-delete"> Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
