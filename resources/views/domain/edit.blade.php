@extends('layout')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Update Domain {{$domain->name}}
                </div>
                <form action="{{route('update', ['id' => $domain->id])}}" method="post">
                    @method('put')
                    @csrf
                    <div class="form-group col-lg-4 col-lg-offset-4">
                        <label>Edit Your Domain</label>
                        <input type="hidden" name="domain" value="{{$domain->id}}">
                        <input type="text" name="name" data-action="edit" class="form-control" value="{{$domain->name}}" autofocus>
                        <p class="help-block domain-info "></p>
                    </div>
                    <div class="col-lg-4 col-lg-offset-4" >
                        <div class="form-group text-center">
                            <button class="btn btn-success buy-domain" type="submit">Save</button>
                        </div>
                    </div>
                </form>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <style>
        .error {
            color: red;
        }

        .success {
            color: green;
        }
    </style>
@endsection
