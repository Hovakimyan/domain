<?php

use App\Http\Models\Domain;
use Illuminate\Database\Seeder;

class DomainSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $domains = ['youtube.com','facebook.com','twitter.com','instagram.com','github.com','bitbucket.org','laravel.com','copart.com'];

        foreach ($domains as $domain)
        {
            $add = new Domain();
            $add->user_id = rand(1,10);
            $add->name = $domain;
            $add->save();
        }
    }
}
