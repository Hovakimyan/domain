<?php

use App\Http\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i = 1; $i <= 10; $i++)
        {
            $user = new User();
            $user->name = 'test user '.$i;
            $user->email = 'test'.$i.'@gmail.com';
            $user->password = Hash::make('111111');
            $user->save();

        }

    }
}
