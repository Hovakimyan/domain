$(document).ready(function () {
    /**
     * Set up csrf token
     */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /**
     * Check domain availability
     */

    var domain_input = $('input[name=name]');

    domain_input.on('keyup',function () {
        checkAvailability($(this));
    });

    function checkAvailability(input)
    {
        var domain_info = $('.domain-info');
        domain_info.removeClass('error').removeClass('success');
        var value = input.val();
        var action = input.data('action');
        var domain = $('input[name=domain]').val();
        if (value.length > 3)
        {
            if (validateDomain(value))
            {
                $.ajax({
                    'url' : '/availability',
                    'type': 'GET',
                    data: {
                        name: value,
                        action: action,
                        domain: domain
                    },

                    success: function (response) {
                        domain_info.removeClass('error').addClass('success').text(response.message);
                        $('.buy-domain').prop('disabled',false)
                    },

                    error: function (errors) {
                        var error = errors.responseJSON;
                        domain_info.addClass('error').removeClass('success').text(error.errors.name)
                        $('.buy-domain').prop('disabled',true)
                    }
                });
            }else{
                domain_info.addClass('error').text('Domain is invalid');
                $('.buy-domain').prop('disabled',true)
            }
        }else {
            domain_info.removeClass('success').addClass('error').text('');
        }
    }

    /**
     * Validate domain
     * @param domain
     * @returns {*}
     */
    function validateDomain(domain) {
        var reg = new RegExp(/^((?:(?:(?:\w[\.\-\+]?)*)\w)+)((?:(?:(?:\w[\.\-\+]?){0,62})\w)+)\.(\w{2,6})$/);
        return domain.match(reg);
    }

    /**
     * Delete domain modal and functions
     */
    $('.delete-domain').click(function () {
        var modal = $('#deleteModal');
        var action = $(this).data('action');
        var method = $(this).data('method');


        if (action && method) {
            $("#deleteDomain").prop('action', action);
            modal.find('input[name=_method]').val(method);
            modal.find('.type').text(method === 'DELETE' ? 'delete' : 'restore');
            $(".delete_submit").prop('disabled', false);
        }
    });




});
