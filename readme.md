# Domain CRUD

In this system you can buy domains, and do some actions with him.

### Prerequisites

You must have 
```
PHP >= 7.1.3
Composer
```

### Installing

After clone you must do these few steps

```
cp .env.example .env
```
```
Create database on your local server called 'domain'
Collation - utf8mb4_general_ci
``` 

####
And set your configs on it
```
.......
DB_HOST=localhost
DB_DATABASE=domain
DB_USERNAME=root
DB_PASSWORD=
.......
```
###
After that 
```
cd /path/to/project
composer install
```

And 

```
php artisan migrate --seed
```

And  run this command on your terminal 
```
php artisan key:generate
php artisan serve
```

### Users for testing

```
username: test1@gmail.com
password: 111111

username: test2@gmail.com
password: 111111
```
