<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;


Route::middleware('guest')->group(function () {
    Route::any('/login', 'AuthController@login')->name('login');
    Route::any('/', 'AuthController@login');
});


Route::middleware('user')->group(function () {
    Route::resource('/domains', 'DomainsController', [
        'names' => [
            'index' => 'index',
            'create' => 'create',
            'store' => 'store',
            'edit' => 'edit',
            'update' => 'update',
            'destroy' => 'destroy',
            'restore' => 'restore'
        ],
    ])
        ->except([
            'show'
        ]);
    Route::patch('/domains/restore/{id}','DomainsController@restore')->name('restore');
    Route::get('/logout', 'AuthController@logout')->name('logout');
});


Route::get('/availability','DomainsController@availability')->name('availability');
