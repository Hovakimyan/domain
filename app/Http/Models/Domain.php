<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/**
 * @property integer user_id
 * @property string name
 */
class Domain extends Model
{
    use SoftDeletes;
    /**
     * @var string
     */
    protected $table = 'domains';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get User Domains
     * @return mixed
     */
    public static function getDomainsByUserId()
    {
        return self::query()
            ->where('user_id', '=', Auth::user()->id)
            ->withTrashed()
            ->get();
    }

    /**
     * Relation for user
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne('App\Http\Models\User');
    }

    /**
     * Find Domain by ID and User ID
     * @param $id
     * @return Model|null|object|static
     */
    public static function findDomainById($id)
    {
        $result = self::query()
            ->where('id', '=', $id)
            ->where('user_id', '=', Auth::user()->id)
            ->first();

        return $result;
    }

    /**
     * Regex for XSS
     * @return string
     */
    public static function regex()
    {
        return '/^[A-ZÀÂÇÉÈÊËÎÏÔÛÙÜŸÑÆŒa-zàâçéèêëîïôûùüÿñæœ0-9_.,() ]+$/';
    }

}
