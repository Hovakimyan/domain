<?php

namespace App\Http\Controllers;

use App\Http\Models\Domain;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DomainsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $domains = Domain::getDomainsByUserId();
        return view('domain.index')->with(compact('domains'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('domain.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->input('name');
        $name = $this->url_to_domain($name);
        $request->replace(array('name' => $name));
        $rules = [
            'name' => 'required:min:3|max:255|unique:domains,name|regex:'.Domain::regex()
        ];

        $messages = [
            'name.unique' => 'This domain already has been taken'
        ];

        $validator = Validator::make($request->all(),$rules,$messages);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($messages)->withInput();
        }

        $domain = new Domain();
        $domain->fill($request->except('_token'));
        $domain->user_id = Auth::user()->id;
        $domain->save();

        $request->session()->flash('success', 'Domain successfully added');

        return redirect('domains');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Models\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function show(Domain $domain)
    {
        //
    }

    /**
     * @param $id
     * @return $this
     */

    public function edit($id)
    {
        $domain = Domain::findDomainById($id);
        return view('domain.edit')->with(compact('domain'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        if ($request->isMethod('put'))
        {
            $rules = [
                'name' => 'required:min:3|max:255|unique:domains,name,'.$id.'|regex:'.Domain::regex()
            ];

            $messages = [
                'name.unique' => 'This domain already has been taken'
            ];

            $validator = Validator::make($request->all(),$rules,$messages);

            if ($validator->fails())
            {
                return redirect()->back()->withErrors($messages)->withInput();
            }

            $domain = Domain::find($id);
            $domain->name = $request->input('name');
            $domain->save();

            $request->session()->flash('success', 'Domain successfully updated');

            return redirect('domains');

        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {

        $domain = Domain::destroy($id);
        if ($domain)
        {
            $request->session()->flash('success', 'Domain successfully deleted');
            return redirect('domains');
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function restore(Request $request,$id)
    {
        $restore = Domain::query()->withTrashed()->find($id)->restore();
        $status = 'danger';
        $message = 'Whoops,something went wrong';
        if ($restore)
        {
            $status = 'success';
            $message = 'Domain restored successfully';
        }
        $request->session()->flash($status, $message);
        return redirect('domains');

    }

    /**
     * Check if domain is available
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function availability(Request $request)
    {
        $except = '';

        if ($request->input('action') == 'edit' && $request->input('domain') > 0)
        {
            $except = ','.$request->input('domain');
        }
        $name = $request->input('name');
        $name = $this->url_to_domain($name);
        $request->replace(array('name' => $name));

        $rules = [
            'name' => 'required:min:3|max:255|unique:domains,name'.$except.'|regex:'.Domain::regex()
        ];

        $messages = [
            'name.unique' => 'This domain already has been taken'
        ];

        $validator = Validator::make($request->all(),$rules,$messages);

        if ($validator->fails())
        {
            return response()->json([
                'success' => false,
                'errors' => $validator->messages()
            ],422);
        }


        return response()->json([
            'success' => true,
            'message' => 'Domain is available'
        ],200);

    }

    /**
     * Remove www http and https from url
     * @param $domain
     * @return null|string|string[]
     */
    public function url_to_domain($domain){
        $input = $domain;

        $input = trim($input, '/');

        if (!preg_match('#^http(s)?://#', $input)) {
            $input = 'http://' . $input;
        }
        $urlParts = parse_url($input);

        $domain = preg_replace('/^www\./', '', $urlParts['host']);

        return $domain;
    }
}
