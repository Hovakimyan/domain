<?php

namespace App\Http\Controllers;



use App\Http\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * Login
     *
     * @param Request $request
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function login(Request $request){
        if ($request->isMethod('post')){

            $rules = [
                'email' => 'required|email',
                'password' => 'required|min:6|max:20'
            ];


            $validator =  Validator::make($request->all(), $rules);
            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $email = $request->input('email');
            $password = $request->input('password');
            if (Auth::attempt(['email' => $email, 'password' => $password])) {
                return redirect('domains');
            }
            return redirect()->back()->withErrors(['password' => 'Incorrect username or password'])->withInput();

        }
        return view('auth.login');
    }


    /**
     * Logout
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(){
        auth()->logout();
        return redirect()->route('login');
    }
}
